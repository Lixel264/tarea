﻿using System;

namespace Clase8
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = 0;
            Console.WriteLine("Dame el primer numero: ");
            num1 = int.Parse(Console.ReadLine());

            int num2 = 0;
            Console.WriteLine("Dame el segundo numero: ");
            num2 = int.Parse(Console.ReadLine());

            if (num1 > num2)
            {
                Console.WriteLine(num1 + " es mayor que " + num2);
            }
            else
            {
                Console.WriteLine(num2 + " es mayor que " + num1);
            }

            Console.ReadKey();

        }
    }
}
