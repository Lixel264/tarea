﻿using System;

namespace Clase13
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            Console.WriteLine("Dame un numero: ");
            num = int.Parse(Console.ReadLine());

            if (num % 4 == 0)
            {
                Console.WriteLine(num + " es multiplo de 4");
            }
            else 
            {
                if (num % 5 == 0)
                {
                    Console.WriteLine(num + " es multiplo de 5");
                }
                else
                {
                    Console.WriteLine(num + " no es multiplo ni de 4 ni de 5 ");
                }
                Console.ReadKey();
            }
        }
    }
}
