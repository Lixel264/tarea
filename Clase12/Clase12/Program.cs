﻿using System;

namespace Clase12
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 0;
            int numero2 = 0;

            Console.WriteLine("Dame el primer numero: ");
            numero = int.Parse(Console.ReadLine());

            Console.WriteLine("Dame el segundo numero: ");
            numero2 = int.Parse(Console.ReadLine());

            if (numero2 != 0)
            {
                Console.WriteLine("El resultado de dividir " + numero + " entre " + numero2 + " es igual a " + numero / numero2);
            }
            else
            {
                Console.WriteLine("ERROR: No se puede dividir entre 0");
            }
            Console.ReadKey();
        }
    }
}
