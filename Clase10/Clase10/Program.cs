﻿using System;

namespace Clase10
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 0;
            Console.WriteLine("Dame un numero: ");
            numero = int.Parse(Console.ReadLine());

            if (numero % 10 == 0)
            {
                Console.WriteLine(numero + " es multiplo de 10");

                int numero2 = 0;
                Console.WriteLine("Dame otro numero: ");
                numero2 = int.Parse(Console.ReadLine());

                if (numero2 % 10 == 0)
                {
                    Console.WriteLine(numero2 + " es multiplo de 10");
                }
                else
                {
                    Console.WriteLine(numero2 + " no es multiplo de 10");
                }
            }
            else
            {
                Console.WriteLine(numero + " no es multiplo de 10");
            }
        }
    }
}
