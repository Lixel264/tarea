﻿using System;

namespace Clase9
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = 0;
            Console.WriteLine("Dame el primer numero: ");
            num1 = int.Parse(Console.ReadLine());

            int num2 = 0;
            Console.WriteLine("Dame el segundo numero: ");
            num2 = int.Parse(Console.ReadLine());

            if (num1 % num2 == 0)
            {
                Console.WriteLine(num1 + " es multiplo de " + num2);
            }
            else
            {
                Console.WriteLine(num1 + " no es multiplo de " + num2);
            }

            Console.ReadKey();

        }
    }
}
