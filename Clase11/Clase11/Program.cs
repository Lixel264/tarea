﻿using System;

namespace Clase11
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 0;
            int numero2 = 0;
            Console.WriteLine("Dame un numero: ");
            numero = int.Parse(Console.ReadLine());

            if (numero == 0)
            {
                Console.WriteLine("El producto de 0 por cualquier numero es igual a 0");
            }
            else
            {
                Console.WriteLine("Dame otro numero: ");
                numero2 = int.Parse(Console.ReadLine());

                Console.WriteLine("El producto de " + numero + " por " + numero2 + " es igual a " + numero * numero2 );
            }
            Console.ReadKey();
        }
    }
}
