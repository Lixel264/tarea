﻿using System;

namespace Clase14
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            int num2 = 0;

            Console.WriteLine("Dame el primer numero: ");
            num = int.Parse(Console.ReadLine());

            Console.WriteLine("Dame el segundo numero: ");
            num2 = int.Parse(Console.ReadLine());

            if (num > 0 && num2 > 0)
            {
                Console.WriteLine("Los dos números son positivos");

            }
            else
            {
                if (num < 0 && num2 < 0)
                {
                    Console.WriteLine("Los dos números son negativos");
                }
                else
                {
                    if (num > 0 && num2 < 0 || num < 0 && num2 > 0)
                    {
                        Console.WriteLine("Uno de los numeros es positivo");
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
