﻿using System;

namespace Clase7
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            Console.WriteLine("Dame un numero: ");
            num = int.Parse(Console.ReadLine());

            if (num % 2 == 0)
            {
                Console.WriteLine(num + " es un numero par");
            }
            else
            {
                Console.WriteLine(num + " no es un numero par");
            }
            Console.ReadKey();
        }
    }
}
